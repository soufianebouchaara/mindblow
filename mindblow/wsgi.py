"""
WSGI config for sarwa project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.10/howto/deployment/wsgi/
"""

import os

from django.core.wsgi import get_wsgi_application
from django.conf import settings
from whitenoise.django import DjangoWhiteNoise

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "mindblow.settings")

application = get_wsgi_application()

if not settings.DEBUG:
	print("Running application in production mode")
	application = DjangoWhiteNoise(application)
else:
	print("Running application in debug mode")
