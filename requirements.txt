Django==1.11.12
gunicorn==19.9.0
whitenoise==3.2.2
psycopg2==2.7.4
dj-database-url==0.4.2